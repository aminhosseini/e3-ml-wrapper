# Copyright (C) 2022  European Spallation Source ERIC

# The following lines are required
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

# Most modules only need to be built for x86_64
ARCH_FILTER += linux-x86_64

# If your module has dependencies, you will generate want to include them like
#
#     REQUIRED += asyn
#     ifneq ($(strip $(ASYN_DEP_VERSION)),)
#       asyn_VERSION=$(ASYN_DEP_VERSION)
#     endif
#
# with $(ASYN_DEP_VERSION) defined in `configure/CONFIG_MODULE`

# Since this file (ml.Makefile) is copied into
# the module directory at build-time, these paths have to be relative
# to that path
APP := meanStdApp
APPDB := $(APP)/Db
APPSRC := $(APP)/src

# If you have files to include, you will generally want to include these, e.g.
#
#     SOURCES += $(APPSRC)/mlMain.cpp
#     SOURCES += $(APPSRC)/library.c
#     HEADERS += $(APPSRC)/library.h
#     USR_INCLUDES += -I$(where_am_I)$(APPSRC)

TEMPLATES += $(wildcard $(APPDB)/*.template)

SOURCES   += $(APPSRC)/meanStdMain.cpp
SOURCES   += $(APPSRC)/meanStd.c
DBDS   += $(APPSRC)/meanStd.dbd

#SCRIPTS += $(wildcard ../iocsh/*.iocsh)



USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)

.PHONY: vlibs
vlibs:
